# Mautic Helper

Mautic Helper is a Chrome Extension for Gmail.
This extension keeps track of the contacts timeline in Mautic. It also adds the option to track emails sent from Gmail.

## Requirements
  - Mautic Helper Chrome Extension
  - Mautic installed on a publicly accessible URL.
  - Gmail account (for email tracking).
 
## How to use
  - Step 1: Install the Mautic Helper Chrome Extension from the Chrome Web Store.
  - Step 2: Gmail Plugin for Mautic
  - Step 3: See Magic

## 1. Step 1: Install the Mautic Helper Chrome Extension
  - Added to Chrome at [here](https://chrome.google.com/webstore/search/mautic).
    [![N|Solid](https://bytebucket.org/duyan9424/mautic_helper_extension/raw/f23713daa78cb493ca36dbc459247b6197ac8851/Mautic_Helper_Extension_Setup_1.png)]()

## 2. Step 2: Configure Gmail Plugin for Mautic
  - Install the Mautic plugin as usual.
    [![N|Solid](https://bytebucket.org/duyan9424/mautic_helper_extension/raw/f23713daa78cb493ca36dbc459247b6197ac8851/Mautic_Helper_Extension_Setup_2.png)]()
  - Click on the Gmail plugin button and enter a secret or key to validate the Mautic Helper Chrome Extension 
    [![N|Solid](https://bytebucket.org/duyan9424/mautic_helper_extension/raw/f23713daa78cb493ca36dbc459247b6197ac8851/Mautic_Helper_Extension_Setup_3.png)]()
  - Configure the extension using the options page.
    [![N|Solid](https://bytebucket.org/duyan9424/mautic_helper_extension/raw/f23713daa78cb493ca36dbc459247b6197ac8851/Mautic_Helper_Extension_Setup_4.png)]()

## 3. Step 3: See Magic
  - On the Chrome browser there will be a button that will notify of new events on the contacts timeline.
    [![N|Solid](https://bytebucket.org/duyan9424/mautic_helper_extension/raw/f23713daa78cb493ca36dbc459247b6197ac8851/Mautic_Helper_Extension_Setup_5.png)]()
  - To track an email sent to a lead, click the Track Email button on the Compose window in Gmail.
    [![N|Solid](https://bytebucket.org/duyan9424/mautic_helper_extension/raw/f23713daa78cb493ca36dbc459247b6197ac8851/Mautic_Helper_Extension_Setup_6.jpg)]()
